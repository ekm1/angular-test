export interface IUser {
  id: number;
  name: string;
  surname: string;
  company: string;
  jobTitle: string;
  mobilePhone: number;
  email: string;
}
