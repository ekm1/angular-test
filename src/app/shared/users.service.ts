import { Injectable } from "@angular/core";
import { IUser } from "./users.model";
import { Subject, Observable } from "rxjs";
@Injectable()
export class UsersService {
  //Get all Users
  getUsers(): Observable<IUser[]> {
    let subject = new Subject<IUser[]>();
    setTimeout(() => {
      subject.next(fakes);
      subject.complete();
    });

    return subject;
  }

  addUser(data) {
    console.log(data);
    if (data.id > fakes.length) {
      fakes.push(data);
      console.log(fakes);
    }
    this.rearrangeIds();
  }
  //Delete User by ID
  deleteUser(id) {
    fakes.filter((value, index) => {
      if (value.id === id) {
        fakes.splice(index, 1);
      }
    });
    this.rearrangeIds();
  }
  //Get User by id for PREV & NEXT
  getUser(id) {
    if (id <= 0 || id >= fakes.length) {
      id = 0;
      return fakes.find(user => user.id === id);
    }
    return fakes.find(user => user.id === id);
  }

  //Rearrange Ids when you delete a user
  rearrangeIds() {
    fakes.forEach((value, index) => {
      value.id = index;
    });
  }
}

const fakes = [
  {
    id: 0,
    name: "Migel",
    surname: "Hoxha",
    company: "Something",
    jobTitle: "Angular Dev",
    mobilePhone: 5235235,
    email: "migel@gmail.com"
  },
  {
    id: 1,
    name: "Joe",
    surname: "Doe",
    company: "Something",
    jobTitle: "Angular Dev",
    mobilePhone: 5235235,
    email: "migel@gmail.com"
  }
];
