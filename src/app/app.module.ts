import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { UsersComponent, UserCardComponent, UsersService } from "./users/index";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UiModule } from "./ui/ui.module";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, UsersComponent, UserCardComponent],
  imports: [BrowserModule, FormsModule, AppRoutingModule, UiModule],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule {}
