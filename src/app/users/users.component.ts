import { Component, OnInit, ViewChild } from "@angular/core";
import { UsersService } from "../shared/users.service";
import { IUser } from "../shared";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"]
})
export class UsersComponent implements OnInit {
  temp: any;
  fakes: IUser[];
  formData: any = {};
  errors: any[] = [];
  formChangesSubscription: any;
  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.usersService.getUsers().subscribe(fakes => {
      this.fakes = fakes;
      this.formData = fakes[0];
    });
  }
  ngAfterViewInit(): void {
    this.formChangesSubscription = this.ngForm.form.valueChanges.subscribe(
      x => {
        if (this.ngForm.form.status === "VALID") {
          this.usersService.addUser(this.formData);
        }
      }
    );
  }
  @ViewChild("userForm") ngForm: any;

  prev() {
    this.temp = this.usersService.getUser(this.formData.id - 1);
    this.formData = this.temp;
  }

  next() {
    this.temp = this.usersService.getUser(this.formData.id + 1);
    this.formData = this.temp;
  }

  add() {
    this.formData = {};
    this.formData.id = this.fakes.length + 1;
  }

  delete() {
    this.usersService.deleteUser(this.formData.id);
    this.formData = {};
    this.formData.id = 0;
  }

  selectData(data) {
    this.formData = data;
  }
}
